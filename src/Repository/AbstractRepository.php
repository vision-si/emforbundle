<?php
namespace emforbfc\AdminBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractRepository extends ServiceEntityRepository {

    /*
     * Fonction de recupération de la data pour l'affichage dans datatable.
     */
    public function queryList($sort, $order, $searchValue, $start, $length, $filtre=[])
    {
        $query = $this->createQueryBuilder('u');

        //////////////////////////////////////////////////////////////
        /// Récupération du nombre total NE PAS TOUCHER
        $queryTotal = clone $query;
        $queryTotal->select('COUNT(1) as nb');
        $resultTotal = $queryTotal->getQuery()->getSingleResult();
        $nbTotal = (isset($resultTotal['nb'])) ? $resultTotal['nb'] : 0;
        ////////////////////////////////////////////////////////////////

        if (!empty($searchValue)) {
            $this->searchInList($query, $searchValue);
        }
        if (!empty($filtre)) {
            $this->filterInList($query, $filtre);
        }

        $this->setInnerJoin($query);

        //////////////////////////////////////////////////////////////
        /// Récupération du nombre total filtré NE PAS TOUCHER
        $queryFiltered = clone $query;
        $queryFiltered->select('COUNT(1) as nb');
        $resultTotalFiltered = $queryFiltered->getQuery()->getSingleResult();
        $nbTotalFiltered = (isset($resultTotalFiltered['nb'])) ? $resultTotalFiltered['nb'] : 0;
        ///////////////////////////////////////////////////////////////

        $this->setOrderBy($query, $sort, $order);
        $query->setFirstResult($start)->setMaxResults($length);

        $return = [];
        $return['nbTotal'] = $nbTotal;
        $return['nbTotalFiltered'] = $nbTotalFiltered;
        $return['results'] = $query->getQuery()->getResult();
        return $return ;
    }

    /*
     * Fonction d'ajout d'InnerJoin dans la query de QueryList.
     */
    private function setInnerJoin(QueryBuilder &$query){
        $this->setMyInnerJoin($query);
        return null;
    }

    /*
     * Fonction de surcharge de setInnerJoin. (A utiliser dans un Repository voulu).
     */
    protected function setMyInnerJoin(QueryBuilder &$query) {
        return null;
    }
    
    /*
     * Fonction d'ajout dun orderBy dans la query de QueryList.
     */
    private function setOrderBy(QueryBuilder &$query, $sort, $order){
        $query->orderBy('u.'.$sort, $order);
        $this->setMyOrderBy($query, $sort, $order);
    }

    /*
     * Fonction de surcharge de setOrderBy. (A utiliser dans un Repository voulu).
     */
    protected function setMyOrderBy(QueryBuilder &$query, $sort, $order) {
        return null;
    }

    /*
     * Fonction de recherche dans le datatable. Ajout d'un andWhere dans la QueryList.
     */
    private function searchInList(QueryBuilder &$query, $searchValue) {
        $query->andWhere('u.id LIKE ?1');
        $query->setParameter(1, $searchValue);

        $this->setMySearchInList($query, $searchValue);
    }

    /*
     * Fonction de surcharge de searchInList. (A utiliser dans un Repository voulu).
     */
    protected function setMySearchInList(QueryBuilder &$query, $searchValue) {
        return null;
    }

    /*
     * filterInList et setMyfilterInList sont des fonctions de filtrage.
     */
    private function filterInList(QueryBuilder &$query, $filtre) {
        foreach($filtre as $key=>$value){
            $this->setMyfilterInList($query, $key, $value);
        }
    }

    protected function setMyfilterInList(QueryBuilder &$query, $key, $value) {
        if (!empty($value)){
            $paramRand = rand(111111111,999999999);

            if(!is_array($value)){
                $query->andWhere('u.'.$key.' = ?'.$paramRand);
                $query->setParameter($paramRand, $value);
            }
            else if (!empty($value[0]) && !empty($value[1]) && date_create_from_format('Y-m-d', $value[0]) && date_create_from_format('Y-m-d', $value[1])) {
                $query->andWhere('u.'.$key.' BETWEEN (:dateOne) AND (:dateTwo)');
                $query->setParameter('dateOne', $value[0]);
                $query->setParameter('dateTwo', $value[1]);
            }
            else {
                $query->andWhere('u.'.$key.' IN (:value)');
                $query->setParameter('value', $value);
            }
        }
    }

}
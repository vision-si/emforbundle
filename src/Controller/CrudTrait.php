<?php


namespace emforbfc\AdminBundle\Controller;

use App\Entity\AbstractEntity;
use App\Entity\Fichier;
use App\Entity\ProfilFonctionnalite;
use App\Entity\Utilisateur;
use App\Service\CheckRuleActionService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use emforbfc\AdminBundle\Functions\CheckDoublonsDbTrait;
use emforbfc\AdminBundle\Functions\ConfigTrait;
use emforbfc\AdminBundle\Functions\CrudTabTrait;
use emforbfc\AdminBundle\Functions\Surcharge;
use emforbfc\AdminBundle\Functions\SurchargeTrait;
use emforbfc\AdminBundle\Functions\TwigTrait;
use emforbfc\AdminBundle\Functions\UploadFileTrait;
use emforbfc\AdminBundle\Functions\Utils;
use emforbfc\AdminBundle\Resources\Form\FiltreType;
use emforbfc\AdminBundle\Service\ContextService;
use PhpParser\Node\Expr\Closure;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Exception;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use emforbfc\AdminBundle\Service\UploadService;

use ReflectionClass;

/**
 * Ce trait est le coeur du bundle. C'est le CRUD principal par lequel passent toutes les actions.
 * Trait CrudTrait
 * @package emforbfc\AdminBundle\Controller
 */
Trait CrudTrait
{
    use SurchargeTrait;
    use CrudTabTrait;
    use ConfigTrait {
        ConfigTrait::__construct as private __ConfigConstruct;
    }
    use TwigTrait;
    use CheckDoublonsDbTrait;
    use UploadFileTrait;

    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    protected $userPasswordEncoder;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * CrudTrait constructor.
     * Ne peut pas être surchargé. Il faut passer par initController() avec "@required"
     * @throws \ReflectionException
     */
    //final public function __construct(ContextService $contextService){
    //final public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager, CheckRuleActionService $checkRule,
    //                                 UserPasswordEncoderInterface $userPasswordEncoder, RouterInterface $router){
    final public function __construct(ContextService $context){

        //Creation des alias pour faciliter les accès
        $this->request = $context->request;
        $this->entityManager = $context->entityManager;
        $this->router = $context->router;
        $this->logger = $context->logger;

        $this->entityShortName = strtolower((new \ReflectionClass(static::ENTITY))->getShortName());
        $this->__ConfigConstruct();
    }


    /**
     * Executé à l'appel du Controller et AVEC AutoWiring :D
     * APRES les required du controller au dessus
     * @required
     */
    final public function initCrud() {

    }

    /*
     * Fonction du rendering de l'index.
     */
    public function renderIndex($getNameRoute, $getConfig, $filtres, $dataForFiltres, $formFiltres) {
        return $this->render($this->getTemplateTwig('index'), array_merge($this->paramToRender(),[
            'config' => $getConfig,
            'filtres' => $filtres,
            'dataForFiltres' => $dataForFiltres,
            'formFiltres' => $formFiltres,
        ]));
    }


    /**
     * Fonction du corps de l'index.
     * @Route("")
     * @return array|object[]
     */
    public function index(Request $request) {
        $findAllDataFiltres = [];

        $getNameRoute = Utils::explodeNameRoute($request->attributes->get('_route'), $this->getPage());
        foreach ($this->getConfFiltres()->getConfArray() as $key => $val) {
            if(isset($val['data'])){
                if(!empty($val['entity'])){
                    $findAllDataFiltres[$key] = Utils::fromObjectToArray($this->basicFindAll($val['entity']));
                } else {
                    $findAllDataFiltres[$key] = $val['data'];
                }
            } else {
                $findAllDataFiltres[$key] = Utils::fromObjectToArray($this->basicFindAll($val['entity']));
            }
        }
        $formFiltres = $this->createForm(FiltreType::class, null, ['filtres'=>$this->getConfFiltres()->getConfArray(), 'dataForFiltres'=>$findAllDataFiltres])->createView();

        return $this->renderIndex($getNameRoute, Utils::setArrayToRender($this->getConfColonne()->getConfArray()), $this->getConfFiltres()->getConfArray(), $findAllDataFiltres, $formFiltres);
    }


    /**
     * Fonction qui reçoit les paramètres POST de datatable et qui retourne un tableau Json avec la data finale d'affichage dans datatable.
     * @Route("list")
     */
    public function indexList(Request $request, CheckRuleActionService $checkRule) {
        $returnData = [];
        $returnDataFinal = [];
        $row = [];
        $entityRepository = $this->entityManager->getRepository(static::ENTITY);
        $getNameRoute = Utils::explodeNameRoute($request->attributes->get('_route'), $this->getPage());

        $draw = $request->request->get('draw');
        $order = $request->request->get('order');
        $columns = $request->request->get('columns');
        $start = $request->request->get('start');
        $length = $request->request->get('length');
        $search = $request->request->get('search');
        $filtre = $request->request->get('filtre');

        parse_str($filtre, $filtre);

        $order = empty($order) ? [["column" => "0", "dir" => "asc"]] : $order;
        $filtre = Utils::array_filter_recursive($filtre);
        $getDataResult = $entityRepository->queryList($columns[reset($order)['column']]['name'], reset($order)['dir'], $search['value'], $start, $length, $filtre);

        $returnDataFinal['draw'] = (int)$draw;
        $returnDataFinal['recordsTotal'] = $getDataResult['nbTotal'];
        $returnDataFinal['recordsFiltered'] = $getDataResult['nbTotalFiltered'];

        foreach ($getDataResult['results'] as $i => $item) {
            $entity = $item->toArray();

            foreach ($this->getConfColonne()->getConfArray() as $key => $opts) {

                $val = $this->getColumnValue($key, $entity, $opts);
                if (is_null($val)) {
                    switch ($key) {
                        case 'actions':
                            if ($checkRule->checkRuleFonctionnalite(strtoupper($this->entityShortName), 2))
                            {
                                $deletable = $item->isDeletable();

                                $class_delete = $data_modal = $data_title = $style_delete = '';
                                if(!$deletable->is){
                                    $class_delete =  'btn-disabled crudModal';
                                    $data_modal =  'data-modal-content="'.$deletable->list.'"';
                                    $data_title = 'Impossible de supprimer';
                                }

                                $val = '<a href=\''.$this->router->generate($getNameRoute.'_'.$this->getPage().'_editnew', ['id'=>$entity['id']]).'\' class=\'btn btn-primary btn-sm\'><i class="fas fa-pencil-alt"></i></a>
                                        <form style="display: inline-block"' . ($deletable->is ? 'method="post" action=\''.$this->router->generate($getNameRoute.'_'.$this->getPage().'_delete', ['id'=>$entity['id']]).'\' onsubmit="return confirm(\'Supprimer ?\')"' : 'action="#"') . '>
                                            ' . ($deletable->is ? '<input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="'.$this->container->get('security.csrf.token_manager')->getToken('delete').'">' : '') . '
                                            <button class="btn btn-sm btn-danger '.$class_delete.'" '. $data_modal.' data-title="'.$data_title.'">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>';

                            } else if ($checkRule->checkRuleFonctionnalite(strtoupper($this->entityShortName), 1)) {
                                $val = '
                                    <a href=\''.$this->router->generate($getNameRoute.'_'.$this->getPage().'_editnew', ['id'=>$entity['id']]).'\' class=\'btn btn-primary btn-sm\'><i class="fas fa-pencil-alt"></i></a>
                                ';
                            } else {
                                $val = '';
                            }
                            break;

                        case 'bulk':
                            $val = '<input type="checkbox" class="checkboxIndexSelect" data-delete-element-url="'.$this->router->generate($getNameRoute.'_'.$this->getPage().'_delete').'" data-delete-bulk-id="'.$entity['id'].'" data-delete-element-token="'.$this->container->get('security.csrf.token_manager')->getToken('delete').'" />';
                            break;
                        default:
                            break;
                    }
                }
                switch($key) {
                    case 'avatar':
                        if(isset($val)) {
                            $val = '<img style="height:34px;border-radius:4px;" alt="Pic" src="/data/'.$val.'">';
                        } else {
                            $val = '<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">'.mb_substr($item->getPrenom(), 0, 1).mb_substr($item->getNom(), 0, 1).'</span>';
                        }
                        break;
                    default:
                        break;
                }
                $row[$key] = $val;
            }
            $returnData[] = $row;
        }

        $returnDataFinal['data'] = $returnData;
        $response = new JsonResponse($returnDataFinal);

        return $response;
    }

    /**
     * Fonction qui appelle l'edit ou new en fonction de la route.
     *
     * @Route("edit/{id}", methods="GET|POST")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function editNew(Request $request, $id = null) {

        $getNameRoute = $this->getPathRoute();

        $entityName = static::ENTITY;
        $entityManager = $this->getDoctrine()->getManager();

        $dataFichiers = [];
        $thisEntity = new $entityName;

        if ($id == null) {

        }  else {
            $thisEntity = $this->entityManager->getRepository($entityName)->find($id);
            $entityFichier = $this->entityManager->getRepository(Fichier::class)->findBy(['entity' => $entityName, 'id_entity' => $id]);
            $dataFichiers = ($entityFichier) ? json_encode(Utils::fromObjectToArray($entityFichier)) : json_encode([]);
        }

        $redirect = $this->editAction($id, $request, $thisEntity);
        if(!empty($redirect)){ return $redirect; }

        $params = array_merge([
            "form" => $this->initializeForm(static::TYPE, $thisEntity, ['id' => $id])->createView(),
            "entity" => $thisEntity,
            "onglets" => $this->getConfOnglets()->getConfArray(),
            'dataFichiers' => $dataFichiers
        ], $this->paramToRender(), $this->getEditParam($thisEntity));

        return $this->render($this->getTemplateTwig('edit'), $params);
    }

    /*
     * Fonction du corps de l'Edit et du New.
     */
    public function editAction($id, Request $request, $entity) {
        $return = null;
        $getFileInputName = null;
        $type = static::TYPE;
        $typeClass = new $type;

        $form = $this->initializeForm(static::TYPE, $entity);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            if($id == null){
                $entity->setUid();
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }

            $typeClass->uploadFileType($form, $this, $entity);

            $this->specificSetFunction($request, $this->entityManager, $entity);

            $this->entityManager->flush();
            $this->addFlash("success", "Modifié avec succès");
            $return = $this->redirectToRoute($this->getRouteActionName('index'));
        }
        return $return;
    }

    /**
     * Fonction de suppresion d'un élement.
     *
     * @Route("delete/{id}", methods="DELETE")
     * @param $id
     * @param Request $request
     * @return
     */
    public function delete($id = null, Request $request)
    {
        $getNameRoute = Utils::explodeNameRoute($request->attributes->get('_route'), $this->getPage());

        if ($id == null) {
            $dataId = $request->get("dataId");

            foreach ($dataId as $data) {
                $this->deleteAction($data['id'], $data['_token']);
            }
            return $this->redirectToRoute($getNameRoute . '_' . $this->getPage() . '_index');
        } else {
            $this->deleteAction($id, $request->get("_token"));
        }
        return $this->redirectToRoute($getNameRoute . '_' . $this->getPage() . '_index');
    }

    public function deleteAction($id, $token) {
        $entity = $this->entityManager->getRepository(static::ENTITY)->find($id);
        $form = $this->createForm(static::TYPE, $entity);

        if ($this->isCsrfTokenValid('delete', $token)) {
            foreach ($form as $key => $val) {
                if (get_class($val->getConfig()->getType()->getInnerType()) == FileType::class) {
                    $getFileInputName = $key;
                    $this->removeFile(true, STATIC::ENTITY, false, null, $id, $getFileInputName);
                }
            }
            try  {
                if($entity->isDeletable()->is) {
                    $this->entityManager->remove($entity);
                    $this->entityManager->flush();
                } else {
                    $this->addFlash('danger', 'Impossible d\'effectuer cette action');
                }
            } catch (DBALException $e) {
                $this->logger->critical($e->getMessage(), ['date' => new \DateTime(), 'declencheur' => $this->request->getUser()]);
                if($this->environment->isDebug()) {
                    $this->addFlash('danger', $e->getMessage());
                }
            }
        }
    }

    /**
     * Retourne toute la data en fonction de l'entité.
     * @param $entity
     * @return array|object[]
     */
    public function basicFindAll($entity) {
        $entityRepository = $this->entityManager->getRepository($entity);
        return $entityRepository->findAll();
    }


    /**
     * @param null $entityName
     * @return string
     */
    protected function getPathRoute($page=null){
        if(is_null($page)){
            $page = $this->getPage();
        }
        return Utils::explodeNameRoute($this->request->attributes->get('_route'), $page);
    }

    protected function getPathController($page=null){
        if(is_null($page)){
            $page = $this->getPage();
        }
        return $this->getPathRoute($page).'_'.$page;
    }
    protected function getRouteActionName($action, $page=null){
        return $this->getPathController($page).'_'.$action;
    }

    /**
     * Retourne le libellé de la page. Si il n'est pas défini on envoi le nom de la page.
     * @return mixed
     */
    protected function getPage(){
        return $this::PAGE; //@todo: remplacer par $this->entityShortName
    }

    /**
     * Retourne le libellé de la page. Si il n'est pas défini on envoi le nom de la page.
     * @return mixed
     */
    protected function getPageLibelle(){
        return is_null($this::PAGE_LIBELLE) ? ucfirst($this->getPage()) : $this::PAGE_LIBELLE;
    }

    /**
     * Tous les paramètres à envoyer tout le temps
     * @return array
     */
    private function paramToRender(){
        return [
            "page" => $this->entityShortName,
            "page_libelle" => $this->getPageLibelle(),
            "routeName" => $this->getPathRoute(),
            'entityName' => $this->entityShortName
        ];
    }

}
<?php


namespace emforbfc\AdminBundle\Functions;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

Trait CheckDoublonsDbTrait
{
    /**
     * @Route("check/doublons/form/{id}", methods="GET|POST")
     */
    public function checkDoublons(Request $request, $id = null) {
        $params = $request->query->all();
        $entityManager = $this->getDoctrine()->getManager();
        $data = null;
        $jsonRep = new JsonResponse();

        if ($id != null) {
            foreach ($params as $key => $param) {
                $thisEntity = "App\Entity\\".ucfirst($key);
                $repository = $entityManager->getRepository($thisEntity);

                $querybuilder = $repository->createQueryBuilder('d');
                $querybuilder->where('d.'.key($param).'= :data')
                    ->andWhere('d.id != :id')
                    ->setParameters(['data' => $param[(string)array_key_first($param)], 'id' => $id]);
                $data = $querybuilder->getQuery()->getResult();

                if ($data != null || !empty($data)) {
                    return $jsonRep->setData(false);
                }
            }
        } else {
            foreach ($params as $key => $param) {
                $thisEntity = "App\Entity\\".ucfirst($key);
                $repository = $entityManager->getRepository($thisEntity);

                $querybuilder = $repository->createQueryBuilder('d');
                $querybuilder->where('d.'.key($param).'= :data')
                    ->setParameters(['data' => $param[(string)array_key_first($param)]]);
                $data = $querybuilder->getQuery()->getResult();

                if ($data != null || !empty($data)) {
                    return $jsonRep->setData(false);
                }
            }
        }

        return $jsonRep->setData(true);
    }
}
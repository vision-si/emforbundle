<?php


namespace emforbfc\AdminBundle\Functions;

use App\Entity\Fichier;
use emforbfc\AdminBundle\Functions\Utils;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Le Trait qui gère l'upload et la suppression des fichiers.
 * Trait UploadFileTrait
 * @package emforbfc\AdminBundle\Functions
 */
Trait UploadFileTrait
{
    /**
     * Gère l'upload d'un élément dans la table de l'entité, et gère aussi l'upload des multifichiers dans la table fichier.
     *
     * @param $file
     * @param $instanceClass
     * @param $notion
     */
    public function upload($instanceClass, $notion, $file) {
        $returnRemove = false;
        $entityName = $this->entityShortName;

        $getFile = 'get' . ucfirst($notion);
        $setFile = 'set' . ucfirst($notion);

        if (is_array($file)) {
            foreach ($file as $fichier) {
                $instanceFichier = new Fichier();
                $mimeType = $fichier->getMimeType();
                $originalFilename = pathinfo($fichier->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);// Le vrai nom du fichier
                $fileName = uniqid().'.'.$fichier->guessExtension();
                $directoryAndFileName = 'upload/'.$entityName.'/'.$fileName;
                $sizeFile = $fichier->getClientSize();

                try {
                    $fichier->move($this->getParameter('files_directory').'/upload/'.$entityName, $fileName);
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $instanceFichier->setUid();
                $instanceFichier->setCreeLe();
                $instanceFichier->setNomOrigine($safeFilename);
                $instanceFichier->setNomUpload($directoryAndFileName);
                $instanceFichier->setLibelle($safeFilename);
                $instanceFichier->setEntity(static::ENTITY);
                $instanceFichier->setIdEntity($instanceClass->getId());
                $instanceFichier->setType($notion);
                $instanceFichier->setMimeType($mimeType);
                $instanceFichier->setTaille($sizeFile);
                $instanceFichier->setSupprUrl($this->router->generate($this->getRouteActionName('removefile')));

                $this->entityManager->persist($instanceFichier);
                $this->entityManager->flush();
            }
        } else {
            if ($file) {
                $mimeType = $file->getMimeType();
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);// Le vrai nom du fichier
                $fileName = uniqid().'.'.$file->guessExtension();
                $directoryAndFileName = 'upload/'.$entityName.'/'.$fileName;

                $getCurrentFile = $instanceClass->$getFile();
                if (!empty($getCurrentFile)) {
                    $this->removeFile(false,  null, true, $getCurrentFile, null, null);
                }

                try {
                    $file->move($this->getParameter('files_directory').'/upload/'.$entityName, $fileName);
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $instanceClass->$setFile($directoryAndFileName);
            }

        }
    }

    /**
     * Suppression d'un élément qui se trouve dans la table d'une entité. Suppression des multifichiers qui se trouvent dans la table fichier.
     *
     * @Route("delete/file/{id}/{nameInput}")
     * @param bool $fileRemoveCrud
     * @param null $file
     * @param null $id
     * @param null $nameInput
     * @return JsonResponse
     */
    public function removeFile($fileRemoveIndex = false, $entityRemove = null, $fileRemoveCrud = false, $file = null, $id = null, $nameInput = null) {
        $jsonRep = new JsonResponse();
        $return = false;

        if ($fileRemoveIndex) {
            $getFile = 'get' . ucfirst($nameInput);
            $instanceEntity = new $entityRemove;
            $entity = $this->entityManager->getRepository($entityRemove)->find($id);
            $queryBuilder = $this->entityManager->createQueryBuilder();

            if (method_exists ($entity, $getFile)) {
                if ($entity->$getFile()) {
                    unlink($this->getParameter('files_directory').'/'.$entity->$getFile());
                }
            } else {
                $entityFichier = $queryBuilder->select('f')->from(Fichier::class, 'f')->where('f.entity = :ent')->andWhere('f.id_entity = :idpar')->setParameters(['ent' => $entityRemove, 'idpar' => $id]);
                $dataFichier = $entityFichier->getQuery()->getResult();

                if (!empty($dataFichier)) {
                    foreach ($dataFichier as $keyF => $val) {
                        unlink($this->getParameter('files_directory').'/'.$val->getNomUpload());
                        $this->entityManager->remove($val);
                        $this->entityManager->flush();
                    }
                }
            }

        } else if (!$nameInput && !$fileRemoveCrud && !$fileRemoveIndex) {
            $entityFile = $this->entityManager->getRepository(Fichier::class)->find($id);
            $fileNameAndDirectory = $entityFile->getNomUpload();

            if ($this->getParameter('files_directory').'/'.$fileNameAndDirectory) {
                $return = unlink($this->getParameter('files_directory').'/'.$fileNameAndDirectory);

                if ($return) {
                    $this->entityManager->remove($entityFile);
                    $this->entityManager->flush();
                    return $jsonRep->setData(true);
                }
            }
        } else {
            if ($fileRemoveCrud) {
                if ($this->getParameter('files_directory').'/'.$file) {
                    $return = unlink($this->getParameter('files_directory').'/'.$file);
                    /*
                    if (Utils::IsDirEmpty($this->getParameter('files_directory').'/upload/'.$entityName)) {
                        rmdir($this->getParameter('files_directory').'/upload/'.$entityName);
                    }
                    */
                }
                return $jsonRep->setData(true);
            } else {
                $entity = $this->entityManager->getRepository(STATIC::ENTITY)->find($id);
                $setFile = 'set'.ucfirst($nameInput);
                $getterFile = 'get'.ucfirst($nameInput);
                $getFile = $entity->$getterFile();

                if ($this->getParameter('files_directory').'/'.$getFile) {
                    $return = unlink($this->getParameter('files_directory').'/'.$getFile);
                    /*
                    if (Utils::IsDirEmpty($this->getParameter('files_directory').'/upload/'.$entityName)) {
                        rmdir($this->getParameter('files_directory').'/upload/'.$entityName);
                    }
                    */
                    if ($return) {
                        $entity->$setFile(null);
                        $this->entityManager->flush();
                        return $jsonRep->setData(true);
                    }
                }
            }
        }
        return $jsonRep->setData(false);
    }


    /**
     * En standby pour l'instant, à ne pas supprimer peut encore servir.
     *
     * @Route("download/file/{fileName}/type/{mimeType}", methods="GET|POST", requirements={"fileName"=".+", "mimeType"=".+"})
     */
   /*  public function downloadFile($fileName, $mimeType) {

         //$response = new Response();
         $file = 'data/'.$fileName;
         $response = new BinaryFileResponse($file);
         $response->headers->set('Content-Type', $mimeType);


         return $response;
     }*/
}
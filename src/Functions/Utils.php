<?php


namespace emforbfc\AdminBundle\Functions;

/**
 * class Utils
*/
class Utils
{
    /*
     * Retourne les configurations pour les colonnes de datatable.
     */
	public static function setArrayToRender($array) {
        $res = [];
        foreach ($array as $key => $item) {
            $searchable = (isset($item['searchable'])) ? $item['searchable'] : true;
            $visible = (isset($item['visible'])) ? $item['visible'] : true;
            $orderable = (isset($item['orderable'])) ? $item['orderable'] : true;
			$width = (isset($item['width'])) ? $item['width'] : ($key == 'actions' ? '150px' : null);

            $res[] = array("data" => $key, "title" => $item['title'], 'name' => $key, 'width'=>$width, 'orderable' => $orderable, 'visible' => $visible, 'searchable' => $searchable);
        }
        return $res;
    }
    
    /*
     * Fonction qui permet de supprimer les éléments vides d'un tableau.
     */
    public function array_filter_recursive($array) {
        foreach ($array as $key => &$value) {
            if (empty($value)) {
                unset($array[$key]);
            }
            else {
                if (is_array($value)) {
                    $value = self::array_filter_recursive($value);
                    if (empty($value)) {
                        unset($array[$key]);
                    }
                }
            }
        }
        return $array;
    }

    /*
     * Récupère le préfixe d'un nom d'une route (exemple: app_admin)
     */
    public static function explodeNameRoute($strNameRoute, $page) {
        $getFinalRouteName = null;
        if(($page !== '')) {
            $getNameArray = array();
            $newArray = [];

            $getNameArray = explode('_', $strNameRoute);

            for ($i = 0; $i < count($getNameArray); $i++) {
                $newArray[] = $getNameArray[$i];
            }

            $getKey = array_search($page, $newArray);

            for ($i = $getKey; $i <= array_key_last($newArray); $i++) {
                unset($newArray[$i]);
            }

            $getFinalRouteName = implode('_', $newArray);
        }
        return $getFinalRouteName;
    }

    /**
     *  Fonction qui met les objets dans un array.
     * @param $objectArray
     * @return array
     */
    public static function fromObjectToArray($objectArray) {
        $finalData = [];
        foreach ($objectArray as $key => $result) {
            $res = $result->toArray();
            $finalData[] = $res;
        }
        return $finalData;
    }

    /**
     * Vérifie si un dossier est vide.
     * @param $dir
     * @return bool
     */
    public function IsDirEmpty($dir) {
        foreach (new \DirectoryIterator($dir) as $fileInfo) {
            if($fileInfo->isDot()) continue;
            return false;
        }
        return true;
    }
    
}
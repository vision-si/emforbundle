function loadKtDatatable(id, type, url, column, param, pageSize = 10, serverPaging = true, serverFiltering = true, serverSorting = true, sortable = true, columnPaging = true) {
    return id.KTDatatable({
        data: {
            type: type,
            source: {
                read: {
                    url: url,
                    method: 'POST',
                    map: function(raw) {
                        if(type === "local") {
                            return url.data;
                        } else {
                            let dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        }
                    }
                },
            },
            pageSize:           pageSize,
            serverPaging:       serverPaging,
            serverFiltering:    serverFiltering,
            serverSorting:      serverSorting,
        },

        // layout definition
        layout: {
            scroll: false,
            footer: false,
        },

        sortable:   sortable,
        pagination: columnPaging,

        search: {
            input: $('#generalSearch'),
        },

        // columns definition
        columns: column,

        translate: {
            records: {
                processing: 'Traitement...',
                noRecords:  'Aucun élément correspondant trouvé',
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first:  'Premier',
                            prev:   'Précédent',
                            next:   'Suivant',
                            last:   'Dernier',
                            more:   'Plus de page',
                            input:  'Numéro de page',
                            select: 'Sélectionnez la taille de la page',
                        },
                        info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments',
                    },
                },
            },
        },

    });
}




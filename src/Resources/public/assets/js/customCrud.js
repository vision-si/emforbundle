
crudIhm = {

    /**
     * Initialisation des fonctions sur toutes les pages twig.
     */
    init: function() {
        this.tabListInit();
        this.filtresInit();
        this.initValidateEachForm();
        this.deleteUploadedElementInit();
        this.multiUploadFilesInit();
        this.initOptionsSelctMultiCheckboxes();
        this.modalInit();
    },




    /********************************  Fonctions utiles  ********************************/
    /***********************************************************************************/


    /******************************************************************************************/
    /******************* Actions sur les checkbox séléctionnées (index) **********************/

    initOptionsSelctMultiCheckboxes: function() {

        $(crudIhm).on('crudIhm.loaded.datatable', function(e){
            let datatable = e.datatable;
            $('th input.selectAllCheckboxesIndex', $(datatable)).on('change', function () {
                $(':checkbox.checkboxIndexSelect').prop('checked', this.checked);
            });
        });

        $('select.selectOptionsMulti').on('change', function () {
            if (this.value === 'deleteSelectedCheckbox') {
                let r = confirm("Etes-vous sûr de vouloir supprimer ?");
                if (r) {
                    crudIhm.deleteElementCheckboxSelected();
                }
            }
        });
    },

    deleteElementCheckboxSelected: function() {
        let checkboxes = document.querySelectorAll('input[type=checkbox].checkboxIndexSelect:checked');
        let deleteUrl;
        let idArray = [];

        $.each(checkboxes, function (index, value) {
            deleteUrl = value.attributes['data-delete-element-url'].value;
            idArray.push({'id':value.attributes['data-delete-bulk-id'].value, '_token': value.attributes['data-delete-element-token'].value});
        });

        $.ajax({
            method: 'DELETE',
            url: deleteUrl,
            data: {'dataId': idArray},
            success: function (data) {
                location.reload();
            },
            error: function () {
            }
        });
    },


    /******************************************************************************************/
    /*********************************** Upload des fichiers *********************************/

    /**
     * Upload de plusieurs fichiers. Cette fonction initialise Bootstrap fileInput. Récupère les images associées à l'entité et à son id.
     * Gère aussi la suppression des images en Ajax.
     */
    multiUploadFilesInit: function() {
        $(".filesInput").each(function(){
            let getId = $(this).attr('id');
            let splitId = getId.split('_');
            let finalId;
            let getData = dataFichiers;
            let dataFiles = [];
            let initialPreview = [];
            let initialPreviewConfig = [];
            let entity;

            /** rm tous les elements du tableau sauf le dernier afin de récupérer le vrai nom de l'input **/
            for( let i = 0; i < splitId.length; i++){
                splitId.splice(i, 1);
            }
            finalId = splitId.toString();

            $.each(getData, function (key, value) {
                if (value.type === finalId) {
                    dataFiles.push(value);
                    initialPreview.push('/data/' + value.nom_upload);
                    entity = ((value.entity).split("\\")).slice(-1)[0];

                    // initialPreviewConfig.push({caption: value.libelle, downloadUrl: '/data/'+value.nom_upload, size: value.taille, url:'/'+entity.toLowerCase()+'/delete/file/'+value.id, key: value.id});
                    initialPreviewConfig.push({
                        caption: value.libelle,
                        downloadUrl: '/data/' + value.nom_upload,
                        size: value.taille,
                        url: value.suppr_url + '/' + value.id,
                        key: value.id
                    });
                }
            });

            $(this).fileinput({
                theme: "fas",
                initialPreview: initialPreview,
                initialPreviewAsData: true,
                initialPreviewConfig: initialPreviewConfig,
                overwriteInitial: false,
            }).on('filebeforedelete', function () {
                !window.confirm('Etes-vous sûr de vouloir supprimer cet élément ?');
            });
        })
    },

    /**
     * Initilisation du click de la suppression de l'élément dans la table de l'entité.
     */
    deleteUploadedElementInit: function() {
        $('.deleteUploadedElement').click(function (e) {
            if (window.confirm('Etes-vous sûr de vouloir supprimer cet élément ?')) {
                crudIhm.deleteUploadedElement($(this));
            }
        });
    },

    /**
     * Suppression de l'élément en Ajax.
     * @param $ctx
     */
    deleteUploadedElement: function($ctx) {
        let deletePath = $ctx.attr('delete-path');

        $.ajax({
            url: deletePath,
            dataType: "json",
        }).done(function (response) {
            $($ctx[0].parentElement).remove();
        }).fail(function (jxh, textmsg, errorThrown) {
        });
    },



    /******************************************************************************************/
    /************** Initialisation du Jquery Validate pour tous les formulaires **************/
    initValidateEachForm: function() {
        $("form").each(function() {
            crudIhm.initValidate(this);
        });
    },

    /******************************************************************************************/
    /******************************** Form filtres Crud ************************************/

    /**
     * Fonction de récupération du click de reset dans le fomulaire des filtres.
     */
    filtresInit: function() {
        $('.formFiltreReset').click(function (e) {
            crudIhm.resetFiltre();
        })
    },

    /**
     * Reset de tous les champs du filtre.
     */
    resetFiltre: function () {
        $("form#filtre select").select2().val('').trigger('change');
        $("form#filtre select[multiple]").select2().val([]).trigger('change');
        $('form#filtre input').val('');
    },



    /******************************************************************************************/
    /******************************** Gestion des onglets ************************************/

    /**
     * Fonction qui détecte les évenements cliqués. Liste d'un onglet, edition, ajout, suppression.
     */
    tabListInit: function(){
        $('ul[role=tablist] li a').on("click", function (event) {
            crudIhm.tabListClick($(this));
        });
    },

    tabListDeleteInit: function(elt){
        $(".formDeleteFormTab" , elt).on("submit", function (e) {
            crudIhm.tabListDeleteClick($(this));
            e.preventDefault();
        });
    },

    /**
     * Fonction de suppression d'un élément dans la liste d'un onglet.
     */
    tabListDeleteClick: function(elt) {
        if(confirm('Êtes vous sûr de vouloir supprimer cet élément ?')) {
            let $form = elt;
            let callback = $form.attr('data-callback');
            $.ajax({
                method: "post",
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: "json",
            }).done(function (response) {
                let fonction;
                if (callback !== undefined) {
                    fonction = new Function(callback + '()');
                    fonction.call(this);
                } else {
                    alert('marche pas');
                }

            }).fail(function (jxh, textmsg, errorThrown) {

            });
        }
    },

    /**
     * Fonction de click sur les onglets qui précharge les différents clicks nouveau / editer.
     *
     * @param id
     * @param url
     * @constructor
     */
    tabListClick: function(elt){

        let id = $(elt).attr('data-id');
        let url = $(elt).attr('data-url');

        let $divTab = $("div#ongl_" + id);
        if ($divTab.html() === '') {
            $divTab.html('Chargement');
            $.ajax({
                method: "post",
                url: url,
                dataType: "html",
            }).done(function (response) {

                $divTab.html(response);

                crudIhm.modalInit($divTab);
                crudIhm.tabListDeleteInit($divTab);

                $('.datatable', $divTab).DataTable();

                $(crudIhm).trigger($.Event('crudIhm.loaded.tab', {target: $("div#ongl_" + id)}));

            }).fail(function (jxh, textmsg, errorThrown) {
                $divTab.html('Erreur');
            });
        }
    },

    /**
     * Fonction de refresh de l'onglet à la suite d'un ajout, edition ou de suppresion d'un élément.
     */
    tabRefresh : function(){
        let $tabActive = $('.nav-tabs[role="tablist"] li a.active');
        let id;
        let href;
        let $tabContentActive;

        if ($tabActive.length === 1) {
            id = $tabActive.attr('data-id');
            href = $tabActive.attr('href');
            $tabContentActive = $('#' + href.replace('#', ''));

            if ($tabContentActive.length === 1) {
                $tabContentActive.html('');
                $tabActive.click();
            }
        }
    },


    /**
     * Initialisation des modèles suite au click. Soit ajout soit édition d'un élément.
     *
     * @param target
     */
    modalInit: function(target){
        if(target  === undefined ){ $(document); }
        $('.crudModal', target).on('click', crudIhm.modalClick);
    },

    /**
     * Ouverture du modal soit pour l'ajout soit pour l'édition. Le body du modèle est rempli ou non par la suite grace à l'appelle de l'ajax qui retourne une réponse.
     */
    modalClick: function(){

        let url = $(this).attr('data-url');
        let title = $(this).attr('data-title');
        let callback = $(this).attr('data-callback');
        let content = $(this).attr('data-modal-content');

        let $modal = $('#modalCrud');
        if($modal.length === 0){

            $('<div class="modal fade" id="modalCrud" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >\n' +
                '                    <div class="modal-dialog modal-dialog-centered" role="document">\n' +
                '                        <div class="modal-content">\n' +
                '                            <div class="modal-header">'+title+'</div>\n' +
                '\n' +
                '                            <div class="modal-body">Chargement</div>\n' +
                '\n' +
                '                            <div class="modal-footer">\n' +
                '                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>').appendTo($('body'));
            $modal = $('#modalCrud');
        } else {
            $('.modal-body', $modal).html('Chargement');
        }

        let data = {url: url, callback: callback, content: content};

        if(content !== undefined){

            let $modalOpened = $modal
                .one('shown.bs.modal', data, function (e) {
                    $('.modal-body', $modal).html(e.data.content);
                    if (callback !== undefined) {
                        crudIhm.modalLoaded($('.modal-body', $modal), e.data);
                    }
                }).one('hidden.bs.modal', data, function (data) {
                    console.log('close');
                }) // only called once
                .modal();

        } else {
            let $modalOpened = $modal
                .one('shown.bs.modal', data, function (e) {
                    $.ajax({
                        method: "get",
                        url: e.data.url,
                        dataType: "html",
                    }).done(function (response) {
                        $('.modal-body', $modal).html(response);
                        if (callback !== undefined) {
                            crudIhm.modalLoaded($('.modal-body', $modal), e.data);
                        }
                    }).fail(function (jxh, textmsg, errorThrown) {
                    });
                }).one('hidden.bs.modal', data, function (data) {
                    console.log('close');
                }) // only called once
                .modal();
        }
    },

    /**
     * Fonction de submit du formulaire. Une fois que le formulaire est chargé, celui-ci sera submit par la suite.
     *
     * @param $target
     * @param data
     */
    modalLoaded: function($target, data){
        let $form = $('form', $target);

        crudIhm.initValidate($form);

        //$form.on('submit', function (e) { //@todo JM : Vlad faut qu'on discute
        $form.validate().settings.submitHandler = function(e){

            if ($form.valid()){
                $.ajax({
                    method: "post",
                    url: $form.attr('action'),
                    data: $form.serialize(),
                    dataType: "json",
                    beforeSend: function () {
                        crudIhm.formSubmitLoaderShow($form);
                    },
                    success: function (response) {
                        let fonction;
                        if (data.callback !== undefined) {
                            fonction = new Function(data.callback + '()');
                            $('#' + $target['prevObject'][0].id).modal('hide');
                            fonction.call(this);
                        } else {
                            $('#' + $target['prevObject'][0].id).modal('hide');
                        }
                        crudIhm.formSubmitLoaderHide($form);
                    },
                    error: function () {

                    }
                })
            }
            return false;
        };
    },

    /**
     * Permet de mettre un loader sur le bouton submit du formulaire
     */
    formSubmitLoaderShow: function(form){
        let $form = $(form);
        let $btnSubmit = $form.find('button[type="submit"]:last');
        if($btnSubmit.length === 1){
            let btnSubmitWidth = $btnSubmit.outerWidth();
            let btnSubmitHtml = $btnSubmit.html();
            $btnSubmit.css('width', btnSubmitWidth+'px')
                .css('text-align', 'center')
                .attr('disabled','disabled')
                .attr('data-html-before', btnSubmitHtml)
                .html('<i class="fa fa-spinner fa-spin"></i>');
        }
        return $btnSubmit;
    },

    /**
     * Permet de cacher le loader sur le bouton submit du formulaire
     */
    formSubmitLoaderHide: function(form){
        let $form = $(form);
        let $btnSubmit = $form.find('button[type="submit"]:last');
        if($btnSubmit.length === 1){
            let btnSubmitHtml = $btnSubmit.attr('data-html-before');
            $btnSubmit.css('width', '')
                .css('text-align', '')
                .removeAttr('disabled')
                .html(btnSubmitHtml);
        }
        return $btnSubmit;
    },

    initValidate: function ($form) {
        $($form).validate({
            invalidHandler: function(event, validator) {
            }
        });
    }

};


/**
 * Appel jquery Validate https://jqueryvalidation.org/
 * exemple :
 * data-rule-required="true"
 * data-rule-email="true"
 * data-rule-url="true"
 * data-rule-date="true"
 * data-rule-dateISO="true"
 * data-rule-number="true"
 * data-rule-digits="true"
 * data-rule-creditcard="true"
 * data-rule-minlength="6"
 * data-rule-maxlength="24"
 * data-rule-rangelength="5,10"
 * data-rule-min="5"
 * data-rule-max="10"
 * data-rule-range="5,10"
 * data-rule-equalto="#password"
 * data-rule-remote="custom-validatation-endpoint.aspx"
 * data-rule-accept=""
 * data-rule-bankaccountNL="true"
 * data-rule-bankorgiroaccountNL="true"
 * data-rule-bic=""
 * data-rule-cifES=""
 * data-rule-creditcardtypes=""
 * data-rule-currency=""
 * data-rule-dateITA=""
 * data-rule-dateNL=""
 * data-rule-extension=""
 * data-rule-giroaccountNL=""
 * data-rule-iban=""
 * data-rule-integer="true"
 * data-rule-ipv4="true"
 * data-rule-ipv6="true"
 * data-rule-mobileNL=""
 * data-rule-mobileUK=""
 * data-rule-lettersonly="true"
 * data-rule-nieES=""
 * data-rule-nifES=""
 * data-rule-nowhitespace="true"
 * data-rule-pattern=""
 * data-rule-phoneNL="true"
 * data-rule-phoneUK="true"
 * data-rule-phoneUS="true"
 * data-rule-phonesUK="true"
 * data-rule-postalcodeNL="true"
 * data-rule-postcodeUK="true"
 * data-rule-require_from_group=""
 * data-rule-skip_or_fill_minimum=""
 * data-rule-strippedminlength=""
 * data-rule-time=""
 * data-rule-time12h=""
 * data-rule-url2=""
 * data-rule-vinUS=""
 * data-rule-zipcodeUS="true"
 * data-rule-ziprange=""
 * ....et d'autres. N'hésitez pas à consulter la documentation.
 */

/**
 * Ajout des méthodes spéciales pour les vérifications sur les champs.
 * Exemple : ajouter dans l'attribut d'un champ dans un Type : 'data-rule-email' => true.
 * Utilisation : 'data-rule-nomDeLaMethode' => true.
 */
if($.isFunction($.fn.validate)){
    /**
     * 2017-11-06 : Suite demande CPRO, surcharge de la méthode "email" native de Jquery validate afin de rajouter test sur TLD (présence obligatoire de .fr, .com etc ... ce qui n'est pas nativement pas obligatoire dans Jquery Validate)
     */
    $.validator.addMethod("email", function(value, element) {
        return this.optional(element)||/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*\.[a-z]{2,63}$/.test(value)
    });

    $.validator.messages.remote = "Cette donnée existe déjà.";
    $.validator.messages.email = "Le format de l'email n'est pas valide.";
    $.validator.messages.required = "Ce champ est obligatoire.";
}




$(function(){
    crudIhm.init();
});
<?php


namespace emforbfc\AdminBundle\Resources\Classes;


class ConfFiltre extends AbstractConf
{
    public $arrayConf = [];

    /**
     * Ajout des éléments dans un tableau pour paramètrer les filtres.
     *
     * @param null $index // Index du tableau qui correspond au nom de colonne dans une table. (Exemples: id, email, nom).
     * @param null $title // Titre du champ du filtre.
     * @param null $type // Type de champ. Les types existants: select / select-multiple / date_interval
     * @param null $entity // L'entité à laquelle le champ est lié pour filtrer.
     * @param null $label // Correspond au nom de colonne dans une table que l'on veut afficher par exemple dans le select. (Exemples: email, nom, cree_le).
     * @param null $dataSend // Correspond à la data qui sera envoyée pour filtrer. (Exemples: email, id, nom).
     * @param null $new_line // Paramètre d'organisation des inputs sur la page. Ajoute ou non une nouvelle ligne (<div class="row">). Valeurs: 0 ou 1.
     * @param null $col_size // Défini la taille du div. (Exemple: <div class="col-md-3">). Valeurs: de 1 à 12.
     * @return $this
     */
    public function add($index = null, $title = null, $type = null, $entity = null, $label = null, $dataSend = null, $new_line = null, $col_size = null) {
        $this->arrayConf[$index] = ['title' => $title, 'type' => $type, 'entity' => $entity, 'label' => $label, 'dataSend' => $dataSend, 'opts' => ['new_line' => $new_line, 'col_size' => $col_size]];
        return $this;
    }
}
<?php


namespace emforbfc\AdminBundle\Resources\Classes;


use emforbfc\AdminBundle\Service\ContextService;

class ConfOnglet extends AbstractConf
{
    public $arrayConf = [];
    public $confColOngletArray = [];

    /**
     * Ajout des éléments dans un tableau pour paramètrer les onglets au sein d'un élément à éditer.
     *
     * @param null $index // Index du tableau qui correspond au nom de colonne dans une table ou au nom d'une entité.
     *                       (Exemples colonne dans une table: id, email, nom /// Exemples entités: action, profil, utilisateur).
     * @param null $action // Nom de l'action pour vérification des droits, si oui ou non l'utilisateur a le droit de consulter l'onglet. (Exemple: 'UTILISATEUR')
     * @param null $title // Titre de l'onglet.
     * @param null $entity // Entité à laquelle correspond l'onglet. (Exemple: Action::class)
     * @param null $template // Chemin vers le template dans le cas d'un onglet particulier à render. (Exemple onglet avec le changement de mot de passe).
     *                          (Exemple chemin: 'Admin/Utilisateur/Actions/password.html.twig');
     * @param null $type // Le Type du fomulaire qui sera utilisé pour ajouter / modifier d'un élément dans un onglet. (Exemple: ActionType::class).
     * @param null $id // l'id qui sera utilisé pour générer l'onglet qui permettra l'identification de celui-ci dans le JS.
     * @param null $route // La route vers le crud du tablist. (Exemple: 'app_admin_fonctionnalite_tablist'. Regarder les routes avec la commande: php bin/console debug:route).
     * @param null $name_fonction // Nom de la fonction qui va s'occuper de récupérer tous les éléments dans un onglet lié à l'entité.
     *                               Par exemple l'entité Fonctionnalite est liée à Action, pour récupérer Action le name_fonction: 'getAction'.
     * @param null $colArray // tableau qui récupére la configuration pour affichier les colonnes du databtale dans l'onglet.
     *                          Il faut utiliser la fonction addCol afin d'ajouter l'index: correspond au nom de colonne dans une table. title: Titre de la colonne.
     * @return $this
     */
    public function add($index = null, $action = null, $title = null, $entity = null, $template = null, $type = null, $id = null, $route = null, $name_fonction = null, $colArray = null) {

        $context = ContextService::getInstance();

        if ($context->getCheckRule()->checkRule($action, 2) || !isset($action)) {
            $this->arrayConf[$index] = ['title' => $title, 'entity' => $entity, 'template' => $template, 'type' => $type, 'id' => $id, 'route' => $route, 'name_fonction' => $name_fonction,
                'conf_col' => $colArray
            ];
        }

        return $this;
    }

    public function addCol($indexCol, $titleCol) {
        $this->confColOngletArray[$indexCol] = ['title' => $titleCol];
        return $this;
    }

    public function getColOngletArray() {
        return $this->confColOngletArray;
    }
}
<?php


namespace emforbfc\AdminBundle\Resources\Classes;


class AbstractConf
{
    /**
     * Retourne le tableau de configuration.
     * @return array
     */
    public function getConfArray() {
        return $this->arrayConf;
    }
}
<?php

namespace emforbfc\AdminBundle\Resources\Classes;

class ConfColonne extends AbstractConf
{
    public $arrayConf = [];

    /**
     * Ajout des éléments dans un tableau pour paramètrer les colonnes.
     *
     * @param null $index // Index du tableau, correspond au nom d'une colonne dans une table (exemples: nom, id, email).
     *                  bulk / actions existent déjà, ce ne sont pas des éléments d'une table.
     * @param null $title // Titre donné aux noms de colonnes.
     * @param null $type // Type de champs (exemples: varchar, int, etc..)
     * @param null $visible // Visibilité du champ. Paramètre de datatable. Valeurs : true ou false.
     * @param null $orderable // Ordonnance d'un champ. Paramètre de datatable. Valeurs: true ou false.
     * @param null $searchable // Recherche sur le champ. Paramètre de datatable. Valeurs: true ou false.
     * @return $this
     */
    public function add($index = null, $title = null, $type = null, $visible = null, $orderable = null, $searchable = null) {
        $this->arrayConf[$index] = ['title' => $title, 'type' => $type, 'visible' => $visible, 'orderable' => $orderable, 'searchable' => $searchable];
        return $this;
    }
}
<?php


namespace emforbfc\AdminBundle\Resources\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbstractCrudType extends AbstractType
{
    public function uploadFileType($form, $thisCtx, $entity) {
        foreach ($form as $key => $val) {
            if (get_class($val->getConfig()->getType()->getInnerType()) == FileType::class) {
                /* @TODO VLAD Quand edit Avatar, supprimé l'ancien */
                $getFileInputName = $key;
                $file = $form[$key]->getData();

                $thisCtx->upload($entity, $key, $file);
            }
        }
    }

    /**
     * MANDATORY : parent::configureOptions($resolver);
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'id' => null
        ]);
    }
}
<?php


namespace emforbfc\AdminBundle\Scripts\Install;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

class Install
{
    public static function postInstall()
    {
        $srcDist = './vendor/emforbfc/adminbundle/src/Resources/public/';
        $dstDist = './public/';

        $srcCrudController = './vendor/emforbfc/adminbundle/src/CrudController.php';
        $dstCrudController = './src/Controller/Admin/CrudController.php';
        //self::recursive($srcDist, $dstDist);
        //self::cpDirectoryAssets();
        self::cpDirectory();
        self::moveFile($srcCrudController, $dstCrudController);
    }

    public static function recursive($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    self::recursive($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    public static function cpDirectory() {
        if (!self::dir_is_empty('./vendor/emforbfc/adminbundle/src/Resources/public/assets')) {
            rename('./vendor/emforbfc/adminbundle/src/Resources/public/assets', './public/assets');
        }
        if (!self::dir_is_empty('./vendor/emforbfc/adminbundle/src/Resources/public/data')) {
            rename('./vendor/emforbfc/adminbundle/src/Resources/public/data', './public/data');
        }

    }

    public static function moveFile($src, $dst) {
        if (file_exists($src)) {
            @mkdir('./src/Controller/Admin/');
            rename($src, $dst);
        }
    }

    public static function dir_is_empty($dirname)
    {
        if (!is_dir($dirname)) return false;
        foreach (scandir($dirname) as $file)
        {
            if (!in_array($file, array('.','..','.svn','.git'))) return false;
        }
        return true;
    }
}


<?php
namespace emforbfc\AdminBundle\Service;

use App\Service\CheckRuleActionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Psr\Log\LoggerInterface;

class ContextService
{
    private static $_instance;

    public $request = null;
    public $entityManager = null;
    public $userPasswordEncoder = null;
    public $router = null;
    public $checkRule = null;
    public $logger = null;

    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager,
                                CheckRuleActionService $checkRule, RouterInterface $router, LoggerInterface $logger){
        $this->request = $requestStack->getCurrentRequest();
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->checkRule = $checkRule;
        $this->logger = $logger;
        self::$_instance = $this;
    }

    /**
     * @param null $request
     */
    public function setRequest($request): void
    {
        $this->request = $request;
    }

    /**
     * @param null $entityManager
     */
    public function setEntityManager($entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param null $router
     */
    public function setRouter($router): void
    {
        $this->router = $router;
    }

    /**
     * @param null $checkRule
     */
    public function setCheckRule($checkRule): void
    {
        $this->checkRule = $checkRule;
    }

    /**
     * @return null
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param null $logger
     * @return ContextService
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return null
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return null
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return null
     */
    public function getRouter()
    {
        return $this->router;
    }

    /**
     * @return null
     */
    public function getCheckRule()
    {
        return $this->checkRule;
    }

    /**
     * La méthode statique qui permet d'instancier ou de récupérer l'instance unique
     **/
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            throw new \Exception('ContextService is not instancied.');
            //self::$_instance = new ContextService(); On peut plus . Le service doit être autowire via le construct du controller
        }
        return self::$_instance;
    }


}